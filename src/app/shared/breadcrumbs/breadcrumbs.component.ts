import { Component, OnInit } from '@angular/core';
import { Router, ActivationEnd } from '@angular/router';
import { Title, MetaDefinition, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styles: []
})
export class BreadcrumbsComponent implements OnInit {
  label: string = '';

  constructor( private router: Router, private title: Title, private meta: Meta ) {
    this.getDataRoute()
      .subscribe( data =>{
        this.label = data.titulo;
        this.title.setTitle( this.label );
        let metaTag: MetaDefinition = {
          name: 'Description',
          content: this.label
        };
        this.meta.updateTag(metaTag);
      });
  }
  getDataRoute(){/** Crear funciones para reducir el codigo en el constructor */
    return this.router.events
      .filter(evento => evento instanceof ActivationEnd)
        .filter((evento: ActivationEnd) => evento.snapshot.firstChild === null )
          .map( (evento: ActivationEnd) => evento.snapshot.data );
  }

  ngOnInit() {
  }

}
