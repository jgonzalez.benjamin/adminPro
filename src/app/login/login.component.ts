import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { UsuarioService } from '../services/service.index';
import { Usuario } from '../models/usuario.model';

declare function init_plugins();
declare const gapi: any;  

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  recuerdame: boolean =  false;
  email:string;
  auth2: any;    

  constructor( private router: Router, private _usuarioService: UsuarioService ) { }

  ngOnInit() {
    init_plugins();
    this.googleInit();
    this.email = localStorage.getItem('email') || '';   
    if (this.email.length > 1) {
      this.recuerdame = true;
    }
  }

  googleInit(){
    gapi.load('auth2', ()=>{
      this.auth2 = gapi.auth2.init({
        client_id: '879341986668-6tqjuv1c80k65ov780seicu173s1tobr.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });

      this.attachSignin(document.getElementById('btn-google'));

    });
  }

  attachSignin( element ){
    this.auth2.attachClickHandler( element, {}, (googleUser)=>{

      /* let profile = googleUser.getBasicProfile(); */
      let token = googleUser.getAuthResponse().id_token;

      this._usuarioService.loginGoogle(token).subscribe( ()=>{
        window.location.href = '#/dashboard'
      });

      
      
    });
  }
  
  ingresar( forma: NgForm){
/*     this.router.navigate(['/dashboard']);*/  

  if (forma.invalid) {
    swal('Oh!','Usuario o contraseña incorrectos','danger');
    return;

  }    

  let usuario = new Usuario(
    null,
    forma.value.email,
    forma.value.password
  );
  this.recuerdame = forma.value.recuerdame;

  this._usuarioService.login(usuario, this.recuerdame).subscribe( 
    res =>this.router.navigate(['/dashboard'])
  );


  }

}
