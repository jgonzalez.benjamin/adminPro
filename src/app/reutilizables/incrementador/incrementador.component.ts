import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-incrementador',
  templateUrl: './incrementador.component.html',
  styleUrls: []
})
export class IncrementadorComponent implements OnInit {
  @ViewChild('txtProgress') txtProgress: ElementRef;
  @Input() porcentaje: number = 50;
  @Input('nombre') leyenda: string = 'Leyenda';

  @Output('actualizar') cambioValorBarra: EventEmitter<number> = new EventEmitter();

  constructor() { 
    
  }

  ngOnInit() {
  }

  onChanges( newValue:number ){
    // let elemHTML:any = document.getElementsByName('porcentaje')[0];
    // console.log(elemHTML.value);

    if(newValue >= 100) {
      this.porcentaje = 100;
    }else if (newValue <= 0) {
      this.porcentaje = 0;
    }else {
      this.porcentaje = newValue;
    } 
    // this.elemHtml = this.porcentaje;
    this.txtProgress.nativeElement.value = this.porcentaje;
    this.cambioValorBarra.emit(this.porcentaje);
  }


  cambiarValor(valor:number){
    if (this.porcentaje >= 100 && valor > 0) {
      this.porcentaje = 100;
      return;
    }
    if (this.porcentaje <= 0 && valor < 0) {
      this.porcentaje = 0;
      return;
    }
    this.porcentaje += valor;
    this.cambioValorBarra.emit(this.porcentaje);
    this.txtProgress.nativeElement.focus();
  }

  
}
