import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class ModalUploadService {

  public type: string;
  public id: string;
  public hidden: string = '';
  public notification = new EventEmitter<any>();

  constructor() {
    console.log('modal service ready');
   }

   ocultarModal(){
    this.hidden = 'oculto';
    this.type = null;
    this.id = null;
   }

   mostrarModal( tipo: string, id:string ){
    this.hidden = '';
    this.id = id;
    this.type = tipo;
   }

}
