import { Component, OnInit } from '@angular/core';
import { SubirArchivoService } from '../../services/service.index';
import { ModalUploadService } from './modal-upload.service';

@Component({
  selector: 'app-modal-upload',
  templateUrl: './modal-upload.component.html',
  styles: []
})
export class ModalUploadComponent implements OnInit {

  hidden: string = 'oculto';
  imagePreview: string;
  imagenSubir: File;

  constructor( private _subirArchivoService: SubirArchivoService, 
    private _modalUploadService: ModalUploadService) { }

  ngOnInit() {
    this.cerrarModal();
  }

  cerrarModal(){
    this.imagenSubir = null; 
    this.imagePreview = null;
    this._modalUploadService.ocultarModal();
    this.hidden = 'oculto';
  }

  subirImagen(){
    let type = this._modalUploadService.type;
    let id = this._modalUploadService.id;
    this._subirArchivoService.subirArchivo(this.imagenSubir, type, id)
    .then( res => {
      this._modalUploadService.notification.emit( res );
      this.cerrarModal();

    }).catch( err => {
      console.log( err );
    });
  }

  seleccionImagen( archivo: File ){

    if (!archivo) {
      this.imagenSubir = null;
      console.log("nop");
      return;
    }
    if (archivo.type.indexOf('image') < 0 ) {
      swal('Only Images','The selected file is not an image','error');
      this.imagenSubir = null;
      console.log("nop2");
      return;
    }


    console.log("intento cargar");
    this.imagenSubir = archivo;

    let reader = new FileReader();

    let urlImagePreview = reader.readAsDataURL( archivo );
    
    reader.onloadend = () => {
      console.log('cargo');
      this.imagePreview = reader.result;
    }

  }

}
