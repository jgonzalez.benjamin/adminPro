import { Pipe, PipeTransform } from '@angular/core';
import { URL_SERVICES } from '../confing/config';

@Pipe({
  name: 'imagen'
})
export class ImagenPipe implements PipeTransform {

  transform(img: string, tipo: string = 'usuario'): any {

    let url = URL_SERVICES + '/img';
    let badUrl = url + '/usuarios/x';

    if (!img) {
      return badUrl;
    }
    if (img.indexOf('https') >= 0) {
      return img;
    }
    switch (tipo) {
      case 'usuario':
        url += '/usuarios/' + img;
        break;
      case 'hospital':
        url += '/hospitales/' + img;
        break;
      case 'medico':
        url += '/medicos/' + img;
        break;
      default:
        console.log('Tipo de imagen no existente: soporta usuarios, hospitales y medicos');
        url = badUrl;
        break;
    }
    return url;
  }

}
