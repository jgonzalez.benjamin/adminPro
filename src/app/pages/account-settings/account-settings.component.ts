import { Component, OnInit, Inject, ElementRef } from '@angular/core';
import { SettingsService } from '../../services/service.index';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styles: []
})
export class AccountSettingsComponent implements OnInit {

  constructor( private _sService:SettingsService ) { }

  ngOnInit() {
    this.colocarChek();
  }

  cambiarColor(theme:string, link:any){
    this.aplicarChek(link);
    this._sService.aplicarTema(theme);
    
  }
  aplicarChek(link:any){
    let selectores:any = document.getElementsByClassName('selector');
   
    for (let ref of selectores) {
      ref.classList.remove('working');
    }
    link.classList.add('working');
    
  }
  colocarChek(){
    let selectores:any = document.getElementsByClassName('selector');
    let tema = this._sService.ajustes.tema;
    for (let ref of selectores) {
      if( ref.getAttribute('data-theme') === tema ){
        ref.classList.add('working');
        break;
      }
     } 

  }

}
