import { Component, OnInit, OnDestroy } from '@angular/core';
import {  Subscription } from 'rxjs/Subscription';
import {  Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: []
})
export class RxjsComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  constructor() { 
    this. subscription = this.regresaObservable()
      .subscribe(  
        numero => console.log( 'Subs: ', numero ),
        error => console.error('Error en el obs ', error),
        () => console.log('El observador termino!')
      );
  }

  ngOnInit() {
  }

  ngOnDestroy(){
    /** Cuando se va cambia de pagina como que se 'destruye' */
    console.log('Oh!, la pág se va a cerrar  :( .. unsuscribe(): activated.');
    this.subscription.unsubscribe();
  }

  regresaObservable(): Observable<any> {
    return new Observable( observer => {
      let contador = 0;
      let intervalo = setInterval( ()=>{
        contador++;
        let salida = {
          valor: contador
        };
        observer.next(salida);
        /** if (contador === 3) {*//** Comentamos todo para probar la funcion de ngOnDestroy() */
        /** clearInterval( intervalo );*//** una forma de hacerlo */
        /** observer.complete();*/
        /** }*/

        // if (contador === 2) { /** Manejo del error */
        //   observer.error('Auxilio');
        // }
      }, 500);
    })
    .retry(2)/** El operador 'retry()' */
      .map( (resp: any) =>{/** El operador 'map()' se puede poner en cualquier parte del observable */
        return resp.valor;
      })
        .filter( (valor, index) =>{/** el operador 'filter()' siempre devuelve un booleano */
        /** el segundo parametro es opcional 'filter((valor,index)=>{console.log('Filter: ', valor, index)})' */
          if ((valor % 2) === 1) {
            /** impar */
            return true;
          }else{
            /** par */
            return false;
          }  
          /** otra forma corta de hacerlo es poniendo 'return ((valor%2)===1);' */
        });
  }

}
