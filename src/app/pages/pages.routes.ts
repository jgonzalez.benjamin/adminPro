
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { ProgressComponent } from './progress/progress.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Graphics1Component } from './graphics1/graphics1.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PromesasComponent } from './promesas/promesas.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { LoginComponent } from '../login/login.component';
import { LoginGuardGuard, AdminGuard, VerificaTokenGuard } from '../services/service.index';
import { ProfileComponent } from './profile/profile.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { HospitalesComponent } from './hospitales/hospitales.component';
import { MedicosComponent } from './medicos/medicos.component';
import { MedicoComponent } from './medicos/medico.component';
import { BusquedaComponent } from './busqueda/busqueda.component';

const PagesRoutes: Routes = [
    { 
        path: 'dashboard',    
        canActivate: [ VerificaTokenGuard ],
        component: DashboardComponent, 
        data: { titulo: 'Dashboard' } 
    },
    { path: 'progress', component: ProgressComponent, data: { titulo: 'ProgressBars' } },
    { path: 'graphics1', component: Graphics1Component, data: { titulo: 'Grphics' } },
    { path: 'promesas', component: PromesasComponent, data: { titulo: 'Promises' } },
    { path: 'rxjs', component: RxjsComponent, data: { titulo: 'RxJS' } },
    { path: 'account-settings', component: AccountSettingsComponent, data: { titulo: 'Theme settings' } },
    { path: 'profile', component: ProfileComponent, data: { titulo: 'Profile' } },
    { path: 'busqueda/:termino', component: BusquedaComponent, data: { titulo: 'search...' } },
    
    /* Mantenimintos */
    { 
        path: 'usuarios', 
        component: UsuariosComponent, 
        canActivate: [AdminGuard],
        data: { titulo: 'Mantenimiento de usuarios' 
    } },

    { path: 'hospitales', component: HospitalesComponent, data: { titulo: 'Mantenimiento de hospitales' } },
    { path: 'medicos', component: MedicosComponent, data: { titulo: 'Mantenimiento de médicos' } },
    { path: 'medico/:id', component: MedicoComponent, data: { titulo: 'Actualizar Médico' } },
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' }
    
];

// tslint:disable-next-line:eofline
export const PAGES_ROUTES = RouterModule.forChild(PagesRoutes);