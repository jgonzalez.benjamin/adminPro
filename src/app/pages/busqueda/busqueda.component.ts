import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICES } from '../../confing/config';
import { Usuario } from '../../models/usuario.model';
import { Medico } from '../../models/medico.model';
import { Hospital } from '../../models/hospital.model';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styles: []
})
export class BusquedaComponent implements OnInit {

  usuarios: Array<Usuario>[] = [];
  medicos: Array<Medico>[] = [];
  hospitales: Array<Hospital>[] = [];

  constructor( private activatedRouter: ActivatedRoute, private _http: HttpClient ) { 
    activatedRouter.params.subscribe( params => {
      let termino = params.termino;
      console.log( termino );
      this.buscar(termino);
    });
  }

  ngOnInit() {
  }

  buscar( termino: string ){
    let url = URL_SERVICES + '/busqueda/todo/' + termino;
    this._http.get(url).subscribe( (res: any) => {
      console.log( res );
      this.usuarios = res.usuarios;
      this.medicos = res.medicos;
      this.hospitales = res.hospitales;
    });
  }

}
