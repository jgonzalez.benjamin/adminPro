import { Component, OnInit } from '@angular/core';
import { HospitalService, UsuarioService } from '../../services/service.index';
import { Hospital } from '../../models/hospital.model';
import { log } from 'util';
import { ModalUploadService } from '../../reutilizables/modal-upload/modal-upload.service';


declare var swal: any;

@Component({
  selector: 'app-hospitales',
  templateUrl: './hospitales.component.html',
  styles: []
})
export class HospitalesComponent implements OnInit {

  hospitales: Array<Hospital>[] = [];
  totalHospitales: number = 0;

  constructor( private _hospitalService: HospitalService , private _usuarioService: UsuarioService,
         private _modalUploadService: ModalUploadService ) { }

  ngOnInit() {
    this.cargarHospitales();
    this._modalUploadService.notification.subscribe( res => this.cargarHospitales() );
  }

  crearHospital(){
    swal({
      text: 'Enter hospital name: ', content: 'input',
    }).then( nombre => {
      this._hospitalService.crearHospital(nombre).subscribe( (res:any) => {
        swal('Exelent! :D', `hospital: ${res.nombre} created`, 'success');
      });
    }).catch((err:any)=>{
      swal('Oh! :(',`${err.message}`,'error');
    });
    console.log('Creado ');
    this.cargarHospitales();
    
  }

  cargarHospitales(){
    this._hospitalService.cargarHospitales().subscribe( (hospitales:any) => {
      this.totalHospitales = hospitales.total;
      this.hospitales = hospitales;
    }); 
  }

  buscarHospital( termino: string ){
    if ( termino.length <= 0 ) {
      this.cargarHospitales();
      return;
    }
    this._hospitalService.buscarHospital(termino).subscribe( (res: Array<Hospital>[]) => {
      this.hospitales = res;
    });
  }

  guardarHospital( hospital: Hospital ){
    this._hospitalService.actualizarHospital(hospital).subscribe( (res) => {

      this.cargarHospitales();
    });
    this.cargarHospitales();
  }

  borrarHospital( hospital: Hospital ){
    if (hospital._id === this._usuarioService.usuario._id) {
      swal('Can not delete user','Can not delete himself','error');
      return;
    }
    swal({
      title: 'Are you sure?',
      text: 'Do you want to delete '+'"'+hospital.nombre+'"'+'?',
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
    .then(willDelete => {      
      if (willDelete) {
        this._hospitalService.borrarHospital(hospital._id).subscribe(()=>{
          this.cargarHospitales();
        });
      }
    });
  }

  mostrarModal(hospital: Hospital ){
    this._modalUploadService.mostrarModal( 'hospitales', hospital._id );
    this.cargarHospitales();
  }



  

}
