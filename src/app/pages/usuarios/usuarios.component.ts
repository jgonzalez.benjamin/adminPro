import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../models/usuario.model';
import { UsuarioService } from '../../services/service.index';
import { ModalUploadService } from '../../reutilizables/modal-upload/modal-upload.service';

declare var swal: any;

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styles: []
})
export class UsuariosComponent implements OnInit {

  usuarios: Array<Usuario>[] = [];
  desde: number = 0;
  totalRegistros: number = 0;
  cargando: boolean = true;

  constructor( private _usuarioService: UsuarioService, private _modalUploadService: ModalUploadService ) { }

  ngOnInit() {
    this.cargarUsuarios();
    this._modalUploadService.notification.subscribe( res => this.cargarUsuarios() );
  }

  mostrarModal( id: string ){
    this._modalUploadService.mostrarModal( 'usuarios', id);
  }

  cargarUsuarios(){
    this.cargando = true;

    this._usuarioService.cargarUsuarios( this.desde ).subscribe( (res: any) => {
      this.totalRegistros = res.total;
      this.usuarios = res.usuarios; 
      this.cargando = false;
    });    
  }
  cambiarDesde( valor: number ){
    let desde = this.desde += valor;

    if (desde >= this.totalRegistros) {
      return;
    }
    if (desde < 0) {
      return;
    }
    this.cargarUsuarios();
    this.desde = desde;
  }

  buscarUsuario(termino: string){
    if (termino.length <= 0) {
      this.cargarUsuarios();
      return;
    }
    this.cargando = true;
    this._usuarioService.buscarUsuario(termino).subscribe( (usuarios: Array<Usuario>[]) => {
      this.usuarios = usuarios;
      this.cargando = false;
    });    
  }

  borrarUsuario( usuario: Usuario ){
    if (usuario._id === this._usuarioService.usuario._id) {
      swal('Can not delete user','Can not delete himself','error');
      return;
    }
    swal({
      title: 'Are you sure?',
      text: 'Do you want to delete '+'"'+usuario.nombre+'"'+'?',
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
    .then(willDelete => {      
      if (willDelete) {
        this._usuarioService.borrarUsuario(usuario._id).subscribe((borrado:boolean)=>{
          console.log(borrado);
          this.cargarUsuarios();
        });
      }
    });
  }

  guardarUsuario( usuario: Usuario ){
    this._usuarioService.actualizarUsuario(usuario).subscribe();
  }

}
