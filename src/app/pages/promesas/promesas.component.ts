import { Component, OnInit } from '@angular/core';
import { error } from 'protractor';

@Component({
  selector: 'app-promesas',
  templateUrl: './promesas.component.html',
  styles: []
})
export class PromesasComponent implements OnInit {

  constructor() { 
  
    // promesa.then(
    //   ()=>console.log('Termino!'), 
    //   ()=>console.error(error)      
    // );
    // es una forma de hacerlo... pero hay una mas "limpia"...

    this.contarTres().then(
        mensaje=>console.log('Termino!', mensaje) // se le puede poner cualquei nnombre a la varieble "mensaje"    
      )
      .catch( error => console.error('Error en la promesa', error)
      );

  }

  ngOnInit() {
  }

  contarTres(): Promise<boolean>{
    return new Promise<boolean>((resolve, reject)=>{
      let contador = 0;
      let intervalo = setInterval(
        ()=>{
          contador+=1;
          console.log(contador);
            
          if (contador === 3) {
            resolve(true);
        //  reject('Simplemente un error en la promesa');
            clearInterval(intervalo);
          }
        }, 1000
      );
    });

  }

}
