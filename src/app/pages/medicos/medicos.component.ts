import { Component, OnInit } from '@angular/core';
import { MedicoService } from '../../services/service.index';
import { Medico } from '../../models/medico.model';
import { ModalUploadService } from "../../reutilizables/modal-upload/modal-upload.service";

@Component({
  selector: 'app-medicos',
  templateUrl: './medicos.component.html',
  styles: []
})
export class MedicosComponent implements OnInit {

 
  medicos: Array<Medico>[] = [];
  totalMedicos: number = 0;

  constructor( private _medicoService: MedicoService, private _modalUploadService: ModalUploadService ) { }

  ngOnInit() {
    this.cargarMedicos();
    this._modalUploadService.notification.subscribe( res => this.cargarMedicos() );
  }


  cargarMedicos(){
    this._medicoService.cargarMedicos().subscribe( res => {
      this.medicos = res.medicos;
    })
    this.totalMedicos = this._medicoService.totalMedicos;
  }

  buscarMedico( termino: string ){
    console.log( termino );

    if (termino.length <= 0) {
      this.cargarMedicos();
      return;
    }
    this._medicoService.buscarMedico( termino ).subscribe( (res: Array<Medico>[]) => {
      this.medicos = res;
    });

  }

  borrarMedico( medico: Medico ){

    this._medicoService.borrarMedico(medico._id).subscribe( res => this.cargarMedicos() );

  }

}
