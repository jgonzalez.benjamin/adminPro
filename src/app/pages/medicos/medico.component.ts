import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Hospital } from '../../models/hospital.model';
import { MedicoService, HospitalService } from '../../services/service.index';
import { ModalUploadService } from '../../reutilizables/modal-upload/modal-upload.service';
import { Medico } from '../../models/medico.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styles: []
})
export class MedicoComponent implements OnInit {

  hospitales: Array<Hospital>[] = [];
  medico: Medico = new Medico('','','','','');
  hospital: Hospital = new Hospital('');

  constructor( private _medicoService: MedicoService, private _hospitalService: HospitalService,
     private _modalUploadService: ModalUploadService, private router: Router, private activatedRoute: ActivatedRoute ) { 
      activatedRoute.params.subscribe(params => {
        let id = params['id'];
        if (id != 'nuevo') {
          this.cargarMedico( id );
        }
       }); 
     }

  ngOnInit() {
    this._hospitalService.cargarHospitales().subscribe( hospitales => this.hospitales = hospitales );
    this._modalUploadService.notification.subscribe( res => {
      this.medico.img = res.medico.img;
    });
  }

  cargarMedico( id: string ){
    this._medicoService.cargarMedico(id).subscribe((medico:any) =>{
      this.medico = medico;
      this.hospital = medico.hospital;
      this.cambioHospital( this.medico.hospital);
    } );  
 
  }

  guardarMedico( f: NgForm){
    console.log(f);
    if (f.invalid) {
      return;
    }

    this.medico.hospital = f.value.hospital;
 
    this._medicoService.guardarMedico( this.medico ).subscribe( medico => {
      
      this.medico._id = medico._id;
      this.router.navigate(['/medico', medico._id]);
    });

  }

  cambioHospital( id: string ){
    
    this._hospitalService.obtenerHospital(id).subscribe( hospital => { 
      this.hospital = hospital;
    });

  }
  
  mostrarModal(){
    this._modalUploadService.mostrarModal('medicos', this.medico._id);
  }



}
