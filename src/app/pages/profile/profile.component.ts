import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/service.index';
import { Usuario } from '../../models/usuario.model';
import { FormControl } from '@angular/forms';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styles: []
})
export class ProfileComponent implements OnInit {

  usuario: Usuario;
  imagePreview: string;
  imagenSubir: File;

  constructor( private _usuarioService: UsuarioService ) {

    this.usuario = this._usuarioService.usuario;

  }

  ngOnInit() {
    
  }

  guardar( usuario: Usuario ){
    this.usuario._id = localStorage.getItem('id');
    this.usuario.nombre = usuario.nombre;

    console.log(usuario);
    
    if (!this.usuario.google) {
      this.usuario.email = usuario.email;
    }
    console.log(this.usuario.nombre);
    this._usuarioService.actualizarUsuario(this.usuario).subscribe();
    
  }
  
  seleccionImagen( archivo: File ){

    if (!archivo) {
      this.imagenSubir = null;
      return;
    }
    if (archivo.type.indexOf('image') < 0 ) {
      swal('Only Images','The selected file is not an image','error');
      this.imagenSubir = null;
      return;
    }

    this.imagenSubir = archivo;

    let reader = new FileReader();

    let urlImagePreview = reader.readAsDataURL( archivo );
    
    reader.onloadend = () => {
      this.imagePreview = reader.result;
    }

  }

  cambiarImagen(){
    this._usuarioService.cambiarImagen( this.imagenSubir, this.usuario._id);
  }
}
