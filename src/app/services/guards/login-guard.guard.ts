import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UsuarioService } from '../usuario/usuario.service';



@Injectable()
export class LoginGuardGuard implements CanActivate {
  constructor( private router: Router, private _usuarioService: UsuarioService ){}

  canActivate():boolean {

    if(this._usuarioService.estaLogueado()){
      return true;
    }else{
      console.log('Bloqueado por el Guard');
      this.router.navigate(['/login']);
      return false;
    }
  }
}
