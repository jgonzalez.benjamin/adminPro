import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICES } from '../../confing/config';  
import { UsuarioService } from '../usuario/usuario.service';
import { Medico } from '../../models/medico.model';
import { ModalUploadService } from '../../reutilizables/modal-upload/modal-upload.service';

@Injectable()
export class MedicoService {

  totalMedicos: number = 0;
  token: string;

  constructor( private _http: HttpClient, private _usuarioService: UsuarioService, private _modalUploadService: ModalUploadService ) { }

  cargarMedicos(){
    let url = URL_SERVICES + '/medico';
    return this._http.get(url).map( (res: any) => {
        this.totalMedicos = res.total;
        return res;
    });
  }

  buscarMedico( termino: string ){

    let url = URL_SERVICES + '/busqueda/coleccion/medicos/' + termino;

    return this._http.get( url ).map( (res:any) => res.medicos );
  }

  borrarMedico( id: string ){
    this.token = this._usuarioService.token; 
    let url = URL_SERVICES + '/medico/' + id;
    url+= '?token=' + this.token;
    return this._http.delete(url).map( res => {
      swal('Poof! medico has been deleted!', {
        icon: "success",
      });
      return res;
    });

  }

  guardarMedico( medico: Medico ){
    let url = URL_SERVICES + '/medico?token=' + this._usuarioService.token;

    return this._http.post( url, medico ).map( (res: any) => {
        swal('Poof! '+ medico.nombre +' has been created!', {
          icon: "success",
        });
        
        return res.medico;
      });
  }

  cargarMedico( id: string){
    let url = URL_SERVICES + '/medico/'+ id;

    return this._http.get(url).map( (res:any) => res.medico );
  }

  mostrarModal( medico: Medico ){
    this._modalUploadService.mostrarModal( 'medicos', medico._id );
    this.cargarMedico(medico._id);
  }

}
