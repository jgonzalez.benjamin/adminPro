import { Injectable } from '@angular/core';
import { URL_SERVICES } from '../../confing/config';

@Injectable()
export class SubirArchivoService {

  constructor() { }

  subirArchivo(archivo: File, tipo: string, id: string){

    return new Promise( (resolve, reject) => {

      let formData = new FormData();
      let xhr = new XMLHttpRequest();
  
      formData.append('imagen', archivo, archivo.name);
  
      xhr.onreadystatechange = function(){
    console.log("xhr",xhr);
        if (xhr.readyState === 4) {
          console.log('entro al primer if: '+ xhr.readyState);
          if (xhr.status === 200) {
            resolve( JSON.parse( xhr.response ) );
          }else{
            console.log('fallo en la subida');
            reject( xhr.response );
          }
        }
      };

      let url = URL_SERVICES + '/upload/' + tipo + '/' + id;

      xhr.open('PUT', url);
      xhr.send( formData );


    });


  }

}
