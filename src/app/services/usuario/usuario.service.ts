import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import swal from 'sweetalert';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Usuario } from '../../models/usuario.model';
import { URL_SERVICES } from '../../confing/config';
import { SubirArchivoService } from '../subir-archivo/subir-archivo.service';

@Injectable()
export class UsuarioService {

  usuario: Usuario;
  token: string;
  menu: Array<any>[];

  constructor( private router:Router, private http: HttpClient, private _subirArchivoService: SubirArchivoService ) {
    this.cargarStorage();
  }
  
  renuevaToken(){
    let url = URL_SERVICES + '/login/renuevatoken';
    url += '?token=' + this.token;

    return this.http.get( url ).map( (resp:any) => {
        this.token = resp.token;
        localStorage.setItem('token', this.token);
        return true
      }).catch( err => {
        this.router.navigate(['/login']);
        swal('No se pudo renovar token', err.message, 'danger');
        return Observable.throw(err);
      });
  }  

  cargarStorage(){
    if (localStorage.getItem('token')) {
      this.token = localStorage.getItem('token');
      this.usuario = JSON.parse(localStorage.getItem('usuario'));
      this.menu = JSON.parse(localStorage.getItem('menu'));
    }else{
      this.token = '';
      this.usuario = null;
      this.menu = [];
    }
  }

  estaLogueado(){
    this.cargarStorage();
    return ( this.token.length > 5)? true: false;
  }

  guardarStorage(id: string, token: string, usuario: Usuario, menu: any){
    localStorage.setItem('id', id);
    localStorage.setItem('token', token);
    localStorage.setItem('usuario', JSON.stringify( usuario ));
    localStorage.setItem('menu', JSON.stringify( menu ));
    this.usuario = usuario;
    this.token = token;
    this.menu = menu;
  }

  logout(){
    this.usuario = null;
    this.token = '';
    this.menu = [];

    localStorage.removeItem('token');
    localStorage.removeItem('usuario');
    localStorage.removeItem('menu');
    
    this.router.navigate(['/login']);
  }

  loginGoogle(token:string){
    let url = URL_SERVICES + '/login/google';
    return this.http.post(url, {token}).map((res:any)=>{
      this.guardarStorage(res.id, res.token, res.usuario, res.menu);
      console.log( res );
      return true;
    });
  }

  login( usuario: Usuario, recordar: boolean){
    if (recordar) {
      localStorage.setItem('email', usuario.email);
    }else{
      localStorage.removeItem('email');
    }
    let url = URL_SERVICES + '/login';
    return this.http.post(url,usuario).map((res: any) => {
      this.guardarStorage(res.id, res.token, res.usuario,res.menu);
      return true;
    }).catch( err => {
      return Observable.throw( err );
    });
  }

  crearUsuario( usuario: Usuario){
    let url = URL_SERVICES + '/usuario';
    return this.http.post(url, usuario).map( (res: any) =>{
      swal('Bien', usuario.nombre + ' estas registrado', 'success');
      return res.usuario;
    });
  }

  actualizarUsuario( usuario: Usuario ){

    let url = URL_SERVICES + '/usuario/' + usuario._id;
    url += '?token=' + this.token;

    console.log(url);

    return this.http.put(url, usuario).map((res:any)=>{
      console.log('entro');
      if (usuario._id === this.usuario._id) {
        let usuarioDB: Usuario = res.usuario;
        this.guardarStorage(usuarioDB._id, this.token, usuarioDB, this.menu);
      }
      swal('Ok! Updated profile', usuario.nombre, 'success');
      return true;
    });
  }
  cambiarImagen( archivo: File, id: string ){
    this._subirArchivoService.subirArchivo( archivo, 'usuarios', id)
    .then( (resp: any) => {
      this.usuario.img = resp.usuario.img;
      swal('Updated Image', this.usuario.nombre , 'success');

      this.guardarStorage(id,this.token,this.usuario, this.menu);    
    }).catch( resp => {
      swal('Oh! :(','A Unspected Error','error');
      console.log( resp ); 
    });
  }

  cargarUsuarios( desde: number = 0 ){
    let url = URL_SERVICES + '/usuario?desde=' + desde;
    return this.http.get( url );
  }

  buscarUsuario(termino: string){
    let url = URL_SERVICES + '/busqueda/coleccion/usuarios/' + termino;
    return this.http.get( url ).map( (res:any) => res.usuarios);
  }
  borrarUsuario(id: string){
    let url = URL_SERVICES + '/usuario/'+id+'?token='+this.token;
    return this.http.delete(url).map( res=>{
      swal('Poof! user has been deleted!', {
        icon: "success",
      });
      return true;
    });
  }

}
