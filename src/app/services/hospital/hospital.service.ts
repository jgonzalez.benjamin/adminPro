import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICES } from '../../confing/config';
import { Hospital } from '../../models/hospital.model';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable()
export class HospitalService {

  totalHospitales: number = 0;
  token: string;

  constructor( private http: HttpClient, private _usuarioService: UsuarioService ) { }

  cargarHospitales(){
    let url = URL_SERVICES + '/hospital';
  
    return this.http.get(url).map( (res: any) => {
      this.totalHospitales = res.total;
      return res.hospitales;
    });
  }

  obtenerHospital( id: string ){
    let url = URL_SERVICES + '/hospital/' + id;

    return this.http.get( url ).map( (res: any) => res.hospital );
  }

  borrarHospital( id: string ){
    this.token = this._usuarioService.token;
    let url = URL_SERVICES + '/hospital/' + id;
    url += '?token=' + this.token;
    this.token = this._usuarioService.token;
    return this.http.delete(url).map( res => swal('OK! ', 'Hospital deleted successfully','success' ) );
  }

  crearHospital( nombre: string ){
    this.token = this._usuarioService.token;
    let url = URL_SERVICES + '/hospital?token=' + this.token;
    return this.http.post(url, { nombre }).map( (res: any) => res.hospital );
  }

  buscarHospital( termino: string ){
    let url = URL_SERVICES + '/busqueda/coleccion/hospitales/' + termino;
    return this.http.get(url).map( (res: any) => res.hospitales );
  }

  actualizarHospital( hospital: Hospital ){
    let url = URL_SERVICES + '/hospital/' + hospital._id;
    this.token = this._usuarioService.token;
    url += '?token=' + this.token;
    return this.http.put(url, hospital).map( (res: any) => { 
      swal('OK! ','Hospital updated successfully!','success');
      return res.hospital; 
    });
  }
}
